#!/bin/bash

#This script demonstrates how modifying the IFS variable can lead to issue

#Global VARs
TEMP="default"
COUNT="1"

#Script starts

while read -r CPU RAM DISK; do
    echo Machine "$COUNT": CPU is "$CPU" -- RAM is "$RAM" -- DISK is "$DISK"
    COUNT=$(( "$COUNT" + 1 ))
done
