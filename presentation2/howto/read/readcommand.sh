#!/bin/bash

#The -r flag make read command interpret the '\' character as a normal part of the string with no special meaning
while read -r VAR; do
	echo line: "$VAR"
done;
