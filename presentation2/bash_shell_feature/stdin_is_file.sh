#!/bin/bash

#This script is for demonstration for bash_shell_feature


#Global vars
USER_INPUT=""
COUNT="0"
#Script starts

while [[ "$COUNT" -lt 10  ]]; do 
    echo proces\'s name is "$0"
    echo Type something and enter
    read USER_INPUT
    echo Your input is: "$USER_INPUT"
	COUNT=$(( "$COUNT" + 1  ))
done
