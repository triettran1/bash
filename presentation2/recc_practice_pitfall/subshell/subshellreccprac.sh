#!/bin/bash

#This script demonstrates how to use Here String instead of pipe to avoid creating subshell in script

#Global VARs
TEMP="default"

#Script starts

while true  LINE; do
    TEMP="$LINE"
    echo Value of TEMP in side loop: "$TEMP"
done <<< $( cat input )

echo Value of TEMP outside loop: "$TEMP";
