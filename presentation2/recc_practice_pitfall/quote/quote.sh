#!/bin/bash

#This script demonstrates an issue with referencing variables without""

#Global vars
FILE_PATH="$1"

#Script starts

echo "Without quotes, this is an error"
echo $( ls -ld $FILE_PATH )

echo $'\n'

echo "With quotes, this is accepted"
echo $( ls -ld "$FILE_PATH" )